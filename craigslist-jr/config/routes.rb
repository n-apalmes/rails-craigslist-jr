Rails.application.routes.draw do

  # root to: "categories#index"
  root "categories#index"

  resources :categories, only: [:index, :show] do
    resources :articles, except: [:edit, :update, :destroy]
  end

  get "/categories/:category_id/articles/:id/edit/:auth_key" => "articles#edit",
  as: "edit_article"

  patch "/categories/:category_id/articles/:id" => "articles#update",
  as: "update_article"

  delete "/categories/:category_id/articles/:id" => "articles#destroy",
  as: "article_delete"

  # get "/categories" => "categories#index"
  # get "/categories/:id" => "categories#show"
  # get "/articles" => "articles#index",
  # as: "article_index"
  # get "/articles/new" => "articles#new"
  # get "/articles/:id" => "articles#show",
  # as: "article_show"
  # post "/articles" => "articles#create"
end
