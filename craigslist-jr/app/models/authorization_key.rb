module AuthorizationKey

  SET_TO_SAMPLE_FROM = ('0'..'9').to_a + ('a'..'z').to_a

  def self.auth_key
    SET_TO_SAMPLE_FROM.sample(5).join
  end

end
