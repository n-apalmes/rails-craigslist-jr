class ArticlesController < ApplicationController

  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params.merge(auth_key: AuthorizationKey.auth_key))

    if @article.save
      redirect_to edit_article_path(@article.category, @article.id, @article.auth_key)
    else
      render :action => "new"
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article= Article.find(params[:id])

    @article.assign_attributes(article_params)

    if @article.save
      redirect_to category_article_path(@article.category,@article)
    else
      render :action => "edit"
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    flash[:success] = "Article deleted"

    redirect_to category_articles_path
  end

  private

  def article_params
    params.require(:article).permit(:title, :description, :category_id)
  end

end

