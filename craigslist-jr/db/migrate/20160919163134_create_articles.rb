class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|

      t.string :auth_key
      t.integer :category_id
      t.string :title
      t.string :description


      t.timestamps null: false
    end
  end
end
