# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# def auth_key_seed
#   auth_key = ''
#   alpha = ("a".."z").to_a
#   3.times { auth_key << alpha.sample }
#   2.times { auth_key << rand(10).to_s }
#   auth_key
# end


Category.create(name: "Community") #1
Category.create(name: "Jobs") #2
Category.create(name: "For Sale") #3
Category.create(name: "Services") #4
Category.create(name: "Gigs") #5
Category.create(name: "Housing") #6

Article.create(category_id: 1, title: "Loner's Meetup", description: "Do you like being along yet hate it? Enjoy being loners with us!", auth_key: AuthorizationKey.auth_key)

Article.create(category_id: 2, title: "Software Engineer", description: "Junior dev needed badly.  $200K!",auth_key: AuthorizationKey.auth_key)
Article.create(category_id: 2, title: "Software QA Engineer", description: "Testers needed. This is not a videogame...",auth_key: AuthorizationKey.auth_key)
Article.create(category_id: 2, title: "Software Project Manager", description: "We need motivators. Stock Options.",auth_key: AuthorizationKey.auth_key)

Article.create(category_id: 3, title: "Gummy bears", description:"fresh out the alley",auth_key: AuthorizationKey.auth_key)
Article.create(category_id: 3, title: "Free Couch", description: "Pick it up from our driveway for FREE.99", auth_key: AuthorizationKey.auth_key)

Article.create(category_id: 4, title: "Maid Service", description: "This is a legit maid service", auth_key: AuthorizationKey.auth_key)

Article.create(category_id: 5, title: "Brewer Needed", description: "My friends and I will pay a brewer for a keg of Pale Ale", auth_key: AuthorizationKey.auth_key)

Article.create(category_id: 6, title: "Roomate Needed", description: "Please respect my space. $700 1bed, shared bathroom",auth_key: AuthorizationKey.auth_key )





